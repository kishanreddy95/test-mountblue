function fifo(stack, order) {
  let temp = [];
  let max = 0;
  let min = 0;
  var tempValue;
  let stackToBeSorted = stack.slice();
  let stackLength = stackToBeSorted.length;
  if (order === 'ascending') {
    while (temp.length !== stackLength) {
      max = 0;
      for (let i = 0; i < stackToBeSorted.length; i++) {
        if (stackToBeSorted[i] >= max) {
          max = stackToBeSorted[i];
        }
      }
      let top = stackToBeSorted.length - 1;
      while (true) {
        if (stackToBeSorted[top] === max) {
          tempValue = stackToBeSorted.pop()
          temp.unshift(tempValue);
          break;
        }
        tempValue = stackToBeSorted.pop();
        stackToBeSorted.unshift(tempValue);
      }
    }
  }
  if (order === 'descending') {
    while (temp.length !== stackLength) {
      min = Infinity;
      for (let i = 0; i < stackToBeSorted.length; i++) {
        if (stackToBeSorted[i] <= min) {
          min = stackToBeSorted[i];
        }
      }
      let top = stackToBeSorted.length - 1;
      while (true) {
        if (stackToBeSorted[top] === min) {
          tempValue = stackToBeSorted.pop()
          temp.unshift(tempValue);
          break;
        }
        tempValue = stackToBeSorted.pop();
        stackToBeSorted.unshift(tempValue);
      }
    }
  }

  console.log(temp);
}

fifo([45, 1, 3, 4, 100], 'descending');
