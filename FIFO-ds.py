from collections import deque

def fifo(stack, order):
  temp = deque()
  stackToBeSorted = deque(stack[0:len(stack)])
  stackLength = len(stackToBeSorted)
  if (order == 'ascending'):
    while (len(temp) != stackLength):
      maxValue = max(stackToBeSorted)
      top = len(stackToBeSorted) - 1
      while True:
        if (stackToBeSorted[top] == maxValue):
          tempValue = stackToBeSorted.pop()
          temp.appendleft(tempValue)
          break
        tempValue = stackToBeSorted.pop()
        stackToBeSorted.appendleft(tempValue)
  if (order == 'descending'):
    while (len(temp) != stackLength):
      minValue = min(stackToBeSorted)
      top = len(stackToBeSorted) - 1
      while True:
        if (stackToBeSorted[top] == minValue):
          tempValue = stackToBeSorted.pop()
          temp.appendleft(tempValue)
          break
        tempValue = stackToBeSorted.pop()
        stackToBeSorted.appendleft(tempValue)
  print(temp)


fifo([45, 1, 3, 4,2, 100], 'descending')