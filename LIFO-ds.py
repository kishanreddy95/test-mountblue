def lifo(stack, order):
  stackToBeSorted = stack[0:len(stack)]
  temp = []
  if (order == 'ascending'):
   while (len(stackToBeSorted) != 0):
      tempAscending = stackToBeSorted.pop()
      if (len(temp) != 0 and temp[len(temp) - 1] > tempAscending):
        stackToBeSorted.append(temp.pop())
      temp.append(tempAscending)
  if (order == 'descending'):
   while (len(stackToBeSorted) != 0):
      tempDescending = stackToBeSorted.pop()
      if (len(temp) != 0 and temp[len(temp) - 1] < tempDescending):
        stackToBeSorted.append(temp.pop())
      temp.append(tempDescending)
  print(temp)
    

lifo([2,3,1,4], 'descending')