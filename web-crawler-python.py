import requests
import re
import json
import csv

r = requests.get('http://www.amazon.in/dp/B01LZKSVRB')
data = r.content.decode('utf-8')

# Setting up pattersn for each data we need from url
titlePattern = re.compile(r'<span id="productTitle" class="a-size-large">[\n\t\s]+([\w\,\(\)\s]+\))')
brandPattern = re.compile(r'<a id="bylineInfo" class="a-link-normal"\s[\w]+=.[\w\/\=\"\s.?&;_-]+>(\w+)')
dealPricePattern = re.compile(r'<span id="priceblock_ourprice" class="a-size-medium a-color-price"><span class="currencyINR">&nbsp;&nbsp;</span> ([\w,.]+)</span>')
mrpPattern = re.compile(r'<span class="a-text-strike"> <span class="currencyINR">&nbsp;&nbsp;</span> ([\w,.]+)</span>')
thumbnailUrlPattern = re.compile(r'<img alt="[\w\s,()]+" src=("[\w\s,:_\-\/.]+")')
numberOfReviewsPattern = re.compile(r'<span id="acrCustomerReviewText" class="a-size-base">([\d,]+) [\w\s]+</span>')
ratingsPattern = re.compile(r'<span class="a-icon-alt">([\d.]+)\sout of 5 stars</span>')
productDetailsPattern = re.compile(r'<li><span class="a-list-item">[\n\s\t]+(\w[\d\w\s:,-|&\(\).]+)[\s\t\n]</span></li>')
metaPattern = re.compile(r'<a class="a-link-normal a-color-tertiary" href="[\w\s\_\-\&\.\?\=\/]+">[\t\n\s]+([\w\s\&]+)\n')

# Matching and extracting data required from data content from url request
title = re.search(titlePattern, data).group(1)
brand = re.search(brandPattern, data).group(1)
deal_price = re.search(dealPricePattern, data).group(1)
mrp = re.search(mrpPattern, data).group(1)
thumbnail_url = re.search(thumbnailUrlPattern, data).group(1)
number_of_reviews = re.search(numberOfReviewsPattern, data).group(1)
rating = re.search(ratingsPattern, data).group(1)
product_details = []
match = re.findall(productDetailsPattern, data)
for match_item in match:
  product_details.append(match_item.replace('\n','').replace('\t',''))
meta = re.findall(metaPattern, data)


final = {
  "title": title,
  "brand": brand,
  "deal_price": deal_price,
  "mrp": mrp,
  "thumbnail_url": thumbnail_url,
  "number_of_reviews": number_of_reviews,
  "rating": rating,
  "product_details": product_details,
  "meta": meta
}

f = csv.writer(open('./test.csv', 'w+'))

h = final.keys()
f.writerow(h)

f.writerow(final.values())
