const rp = require('request-promise');
const cheerio = require('cheerio');
const jsonexport = require('jsonexport');
const fs = require('fs');

const options = {
  url: 'http://www.amazon.in/dp/B01LZKSVRB',
  json: true,
  transform: body => cheerio.load(body),
};

rp(options)
  .then(($) => {
    // Getting data from url
    const title = $('#productTitle').text().replace(/(\s\n*)/g, '');
    const brand = $('#bylineInfo_feature_div').children().eq(0).first()
      .text()
      .replace(/(\s\n*)/g, '')
      .slice(2);
    const deal_price = $('#priceblock_ourprice').text().replace(/\s*/, '');
    const mrp = $('.a-text-strike').text().replace(/\s*/, '').slice(0, 9);
    const no_of_reviews = $('#acrCustomerReviewText').text();
    const thumbnail_url = $('#imgTagWrapperId').children().eq(0).first()
      .attr('data-old-hires');
    const rating = $('#acrPopover').attr('title');
    const product_details = $('#productDescription').find('p').text().replace(/\s+$/, '');

    // Creating an JSON object to convert it to csv
    const product = [{
      title,
      brand,
      deal_price: parseFloat(deal_price.replace(',', '')),
      mrp: parseFloat(mrp.replace(',', '')),
      url: 'http://www.amazon.in/dp/B01LZKSVRB',
      thumbnail_url,
      no_of_reviews,
      rating: parseFloat(rating),
      product_details,
    }];

    // JSON to csv conversion and writing to a csv file
    jsonexport(product, (err, csv) => {
      if (err) {
        return console.log(err);
      }
      fs.writeFileSync('product.csv', csv, 'utf-8', (err) => {
        if (err) {
          console.log(err);
        }
        console.log('success');
      });
    });
  });
