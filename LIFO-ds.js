function lifo(stack, order) {
  var stackToBeSorted = stack.slice();
  var sort = [];
  if (order === 'ascending') {
    while (stackToBeSorted.length !== 0) {
      var tempAscending = stackToBeSorted.pop();
      if (sort.length !== 0 && sort[sort.length - 1] > tempAscending) {
        stackToBeSorted.push(sort.pop());
      }
      sort.push(tempAscending);
    }
  }
  if (order === 'descending') {
    while (stackToBeSorted.length !== 0) {
      var tempDescending = stackToBeSorted.pop();
      if (sort.length !== 0 && sort[sort.length - 1] < tempDescending) {
        stackToBeSorted.push(sort.pop());
      }
      sort.push(tempDescending);
    }
  }
  console.log(sort);
}

lifo([2, 3, 1, 4], 'descending');
